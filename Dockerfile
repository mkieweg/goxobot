FROM scratch
EXPOSE 8080
COPY resources/config.json /resources/config.json
COPY artifacts/goxobot /goxobot
ENTRYPOINT ["/goxobot"]
