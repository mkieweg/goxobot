package handler

import (
	"reflect"
	"testing"
)

func TestNewHandler(t *testing.T) {
	tests := []struct {
		name string
		want Handler
	}{
		{
			name: "Handler",
			want: Handler{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewHandler(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewHandler() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_handler_Handle(t *testing.T) {
	type args struct {
		path string
	}

	tests := []struct {
		name string
		args args
	}{
		{
			name: "testfile",
			args: args{"../resources/testconfig.json"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			target := Handler{}

			if err := target.Handle(tt.args.path); err != nil {
				t.Errorf("Error during test Execution: %s", err)
			}
		})
	}
}
