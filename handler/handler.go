package handler

import (
	"code.fbi.h-da.de/cocsn/goxobot/handleable"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type Handler struct {
}

func NewHandler() Handler {
	return Handler{}
}

func (h Handler) Handle(path string) error {
	f, err := ioutil.ReadFile(path)
	handleables := make([]handleable.Handleable, 0)
	err = json.Unmarshal(f, &handleables)
	for _, hl := range handleables {
		http.HandleFunc(hl.Uri, hl.HandleFunc)
	}
	return err
}
