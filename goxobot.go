package main

import (
	"code.fbi.h-da.de/cocsn/goxobot/handler"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	if _, err := os.Stat("resources/config.json"); err != nil {
		files, _ := ioutil.ReadDir(".")
		for _, file := range files {
			file.Name()
		}
		log.Fatal(err)
	}
	if err := handler.NewHandler().Handle("resources/config.json"); err != nil {
		log.Fatal(err)
	}
	log.Fatal(http.ListenAndServe(":8080", nil))
}
