package handleable

import (
	"fmt"
	"net/http"
)

type Handleable struct {
	Uri     string
	Payload string
	Fields  []HeaderField
}

func (h Handleable) HandleFunc(w http.ResponseWriter, r *http.Request) {
	for _, field := range h.Fields {
		w.Header().Add(field.Key, field.Value)
	}
	fmt.Fprint(w, h.Payload)
}

type HeaderField struct {
	Key   string
	Value string
}
