package handleable

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandleable_HandleFunc(t *testing.T) {
	type fields struct {
		Uri     string
		Payload string
		Fields  []HeaderField
	}
	type args struct {
		w *httptest.ResponseRecorder
		r *http.Request
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{"valid",
			fields{
				Uri:     "test",
				Payload: "{ok}",
				Fields: []HeaderField{
					{
						Key:   "key",
						Value: "value",
					},
				},
			},
			args{
				w: httptest.NewRecorder(),
				r: nil,
			}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := Handleable{
				Uri:     tt.fields.Uri,
				Payload: tt.fields.Payload,
				Fields:  tt.fields.Fields,
			}
			tt.args.r = queryGETWithParameter(tt.fields.Uri)
			h.HandleFunc(tt.args.w, tt.args.r)
			response := readBody(tt.args.w)
			if response != tt.fields.Payload {
				t.Errorf("Test failed. Got %s, expected %s", response, tt.fields.Payload)
			}
		})
	}
}

func queryGETWithParameter(params string) *http.Request {
	req, _ := http.NewRequest("GET", "URL"+params, nil)
	return req
}
func readBody(res *httptest.ResponseRecorder) string {
	content, _ := ioutil.ReadAll(res.Body)
	return string(content)
}
